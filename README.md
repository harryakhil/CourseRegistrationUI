# `CourseRegistrationUI` — Sample app for student course registration

This project is an application for online student course registration. 
Student can view and enroll a course and check the status of the course.

## Getting Started

To get you started you can simply clone the `CourseRegistrationUI` repository and follow below instruction:

### Prerequisites

You need git to clone the `CourseRegistrationUI` repository. 
Install [Node.js](https://nodejs.org/)

**Install dependencies from package.json**


### Clone `CourseRegistrationUI`

Clone the `CourseRegistrationUI` repository using git:


git clone https://gitlab.com/harryakhil/CourseRegistrationUI.git

cd courseRegistration


### Run the Application

In the command prompt run the below command:

**npm run dev**

As we are using **lite-server** it will load the application in the browser.


### Run the tests

**TODO**








