var app = angular.module("OnlineRegistrationApp", ["ngRoute"]);

app.config(function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: './views/home.html',
        controller: 'homeController'
    })
        .when('/dashboard', {
            templateUrl: './views/dashboard.html',
            controller: 'courseController'
        })
        .when('/registrationForm', {
            templateUrl: './views/registrationForm.html',
            controller: 'studentController'
        })
        .when('/status', {
            templateUrl: './views/statusRegistration.html',
            controller: 'studentController'
        })
        .otherwise({
            redirectTo: '/'
        });
});

app.controller('homeController', function ($scope, $location) {
    $scope.goToDashboard = function () {
        $location.path('/dashboard');
    };

    $scope.checkStatus = function () {
        $location.path('/status');
    };

    $scope.registerCourse = function () {
        $location.path('/registrationForm');
    };
});


app.controller('courseController', function ($scope, dataService) {
    $scope.courses = {};
    dataService.getCourses().then(function (dataResponse) {
        $scope.courses = dataResponse.data;
        //console.log(JSON.stringify($scope.courses));
    });
});

app.controller('studentController', function ($scope, $http, $location, dataService) {
    $scope.students = {};
    dataService.getStudents().then(function (dataResponse) {
        $scope.students = dataResponse.data;
        //console.log(JSON.stringify($scope.students));
    });
    dataService.getCourses().then(function (dataResponse) {
        $scope.courses = dataResponse.data;
        $scope.selectedCourse = $scope.courses[0];
        //console.log(JSON.stringify($scope.student));
    });

    $scope.submit = function () {
        var data = {"studentName": $scope.studentName, "address": $scope.address,
                     "course": { "courseId": $scope.selectedCourse.courseId }};

        console.log(JSON.stringify(data));
    
        dataService.registerStudent(data).then(function (dataResponse) {
            $scope.student = dataResponse.data;
            //console.log(JSON.stringify($scope.students));
            $location.path('/status');
        });
    }
});

app.service('dataService', function ($http) {
    delete $http.defaults.headers.common['X-Requested-With'];
    this.getCourses = function (callbackFunc) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8080/courses/'
        });
    };

    this.getStudents = function (callbackFunc) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8080/students/'
        });
    };

    this.registerStudent = function (reqData) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8080/students/',
            data: reqData,
            headers: { 'Content-Type': 'application/json' }
        });
    }
});
